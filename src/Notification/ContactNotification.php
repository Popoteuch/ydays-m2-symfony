<?php
namespace App\Notification;

use App\Document\Contact;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Twig\Environment;

class ContactNotification {

    /**
     * @var MailerInterface
     */
    private $mailer;
    /**
     * @var Environment
     */
    private $renderer;

    public function __construct(MailerInterface $mailer, Environment $renderer)
    {
        $this->mailer = $mailer;
        $this->renderer = $renderer;
    }

    /**
     * @Route("/email")
     */
    public function sendEmail(Contact $contact)
    {
        $email = (new Email())
            ->from($contact->getEmail())
            ->to('aurelien.goulley@gmail.com')
            ->subject('Contact de '.$contact->getFirstname().' '.$contact->getLastname())
            ->replyTo($contact->getEmail())
            ->html($this->renderer->render('emails/contact.html.twig', [
                'contact' => $contact
            ]), 'text/html');

        $this->mailer->send($email);
    }
}