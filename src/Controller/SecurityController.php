<?php

namespace App\Controller;

use App\Document\Product;
use App\Document\User;
use App\Form\Type\UserType;
use App\Repository\ProductRepository;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController {

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var ProductRepository
     */
    private $repository;
    /**
     * @var DocumentManager
     */
    private $dm;

    public function __construct(UserPasswordEncoderInterface $encoder, DocumentManager $dm)
    {
        $this->encoder = $encoder;
        $this->repository = $dm->getRepository(User::class);
        $this->dm = $dm;
    }

    /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils) {
        $lastUsername = $authenticationUtils->getLastUsername();
        $error = $authenticationUtils->getLastAuthenticationError();
        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
            'current_menu' => 'login'
        ]);
    }

    /**
     * @Route("/register", name="register")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function register(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $user->setPassword($this->encoder->encodePassword($user, $user->getPassword()));
            $this->dm->persist($user);
            $this->dm->flush();
            $this->addFlash('success','Inscription réussie.');
            return $this->redirectToRoute('login');
        }

        return $this->render('security/register.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'current_menu' => 'register',
        ]);
    }
}