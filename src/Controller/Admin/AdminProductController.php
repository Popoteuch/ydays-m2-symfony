<?php

namespace App\Controller\Admin;

use App\Repository\ProductRepository;
use Doctrine\ODM\MongoDB\MongoDBException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Twig\Environment;
use App\Document\Product;
use Doctrine\ODM\MongoDB\DocumentManager;
use App\Form\Type\ProductType;

class AdminProductController extends AbstractController
{
    /**
     * @var ProductRepository
     */
    private $repository;
    /**
     * @var DocumentManager
     */
    private $dm;

    public function __construct(DocumentManager $dm)
    {
        $this->repository = $dm->getRepository(Product::class);
        $this->dm = $dm;
    }

    /**
     * @Route("/admin/product", name="admin.product.index")
     */
    public function index(): Response
    {
        $products = $this->repository->findAll();
        return $this->render('admin/product/index.html.twig', [
            'controller_name' => 'AdminProductController',
            'current_menu' => 'admin',
            'products' => $products,
        ]);
    }

    /**
     * @Route("/magasin/{slug}-{id}", name="magasin.show", requirements={"slug": "[a-z0-9\-]*"})
     * @param $slug
     * @param Product $product
     * @return Response
     */
   public function show($slug, Product $product): Response
   {
        if($product->getSlug() !== $slug)
        {
            return $this->redirectToRoute('magasin.show', [
                'id' => $product->getId(),
                'slug' => $product->getSlug()
            ], 301);
        }
        return $this->render('magasin/show.html.twig', [
            'controller_name' => 'MagasinController',
            'current_menu' => 'admin',
            'product' => $product,
        ]);
   }

    /**
     * @Route("/admin/product/create", name="admin.product.new")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function new(Request $request)
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $this->dm->persist($product);
            $this->dm->flush();
            $this->addFlash('success','Ajout fait avec succès.');
            return $this->redirectToRoute('admin.product.index');
        }

        return $this->render('admin/product/new.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
            'current_menu' => 'admin',
        ]);
    }

    /**
     * @Route("/admin/product/{id}", name="admin.product.edit", methods="GET|POST")
     * @param Product $product
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws MongoDBException
     */
    public function edit(Product $product, Request $request)
    {
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $this->dm->flush();
            $this->addFlash('success','Modification faite avec succès.');
            return $this->redirectToRoute('admin.product.index');
        }

        return $this->render('admin/product/edit.html.twig', [
            'controller_name' => 'MagasinController',
            'current_menu' => 'admin',
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/product/{id}", name="admin.product.delete", methods="DELETE")
     * @param Product $product
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws MongoDBException
     */
    public function delete(Product $product, Request $request)
    {
        if($this->isCsrfTokenValid('delete' . $product->getId(), $request->get('_token')))
        {
            $this->dm->remove($product);
            $this->dm->flush();
            $this->addFlash('success','Suppression faite avec succès.');
        }
        return $this->redirectToRoute('admin.product.index');
    }

}
