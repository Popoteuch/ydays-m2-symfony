<?php

namespace App\Controller;

use App\Document\ProductSearch;
use App\Form\Type\ProductSearchType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;
use App\Document\Product;
use Doctrine\ODM\MongoDB\DocumentManager;

class ProductController extends AbstractController
{
    /**
     * @var ProductRepository
     */
    private $repository;

    public function __construct(DocumentManager $dm)
    {
        $this->repository = $dm->getRepository(Product::class);
    }

    /**
     * @Route("/product", name="product.index")
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        $search = new ProductSearch();
        $form = $this->createForm(ProductSearchType::class, $search);
        $form->handleRequest($request);

        $products = $paginator->paginate(
            $this->repository->findAllVisibleQuery($search),
            $request->query->getInt('page', 1),
            16
        );
        return $this->render('product/index.html.twig', [
            'controller_name' => 'ProductController',
            'current_menu' => 'magasin',
            'products' => $products,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/product/{slug}-{id}", name="product.show", requirements={"slug": "[a-z0-9\-]*"})
     * @param $slug
     * @param Product $product
     * @return Response
     */
   public function show($slug, Product $product): Response
   {
        if($product->getSlug() !== $slug)
        {
            return $this->redirectToRoute('product.show', [
                'id' => $product->getId(),
                'slug' => $product->getSlug(),
                'current_menu' => 'magasin',
            ], 301);
        }
        return $this->render('product/show.html.twig', [
            'controller_name' => 'ProductController',
            'current_menu' => 'magasin',
            'product' => $product,
        ]);
   }
}
