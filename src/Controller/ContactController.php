<?php

namespace App\Controller;

use App\Document\Contact;
use App\Form\Type\ContactType;
use App\Notification\ContactNotification;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact")
     */
    public function index(Request $request, ContactNotification $notification): Response
    {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $notification->sendEmail($contact);
            $this->addFlash('success', 'Votre email a bien été envoyé');
            return $this->redirectToRoute('contact', [
                'controller_name' => 'ContactController',
                'current_menu' => 'contact',
                'form' => $form->createView(),
            ]);
        }

        return $this->render('contact/index.html.twig', [
            'controller_name' => 'ContactController',
            'current_menu' => 'contact',
            'form' => $form->createView(),
        ]);
    }
}
