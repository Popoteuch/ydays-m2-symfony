<?php

namespace App\Repository;

use App\Document\ProductSearch;
use Doctrine\ODM\MongoDB\Query\Builder;
use Doctrine\ODM\MongoDB\Query\Query;
use Doctrine\ODM\MongoDB\Repository\DocumentRepository;

class ProductRepository extends DocumentRepository
{
    public function findAllVisibleQuery(ProductSearch $search): Query
    {
        $query = $this->createQueryBuilder('p')
            ->field('stock')->gt(0);
        if($search->getMaxPrice()) {
            $query = $query
                ->field('price')->lte($search->getMaxPrice());
        }
        if($search->getMinStock()) {
            $query = $query
                ->field('stock')->gte($search->getMinStock());
        }
        return $query->getQuery();
    }
}