<?php

namespace App\Document;

class ProductSearch {
    /**
     * @var int|null
     */
    private $maxPrice;

    /**
     * @var int|null
     */
    private $minStock;

    /**
     * @return int|null
     */
    public function getMinStock(): ?int
    {
        return $this->minStock;
    }

    /**
     * @param int|null $minStock
     */
    public function setMinStock(?int $minStock): void
    {
        $this->minStock = $minStock;
    }

    /**
     * @return int|null
     */
    public function getMaxPrice(): ?int
    {
        return $this->maxPrice;
    }

    /**
     * @param int|null $maxPrice
     */
    public function setMaxPrice(?int $maxPrice): void
    {
        $this->maxPrice = $maxPrice;
    }
}