<?php

// src/Document/Product.php
namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Cocur\Slugify\Slugify;
use App\Repository\ProductRepository;

/**
 * @MongoDB\Document(repositoryClass=ProductRepository::class)
 */
class Product
{
    const TYPE = [
        0 => "Fruit",
        1 => "Légume",
        2 => "Viande",
        3 => "Poisson",
        4 => "Féculent",
        5 => "Biscuit",
    ];

    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $name;

    /**
     * @MongoDB\Field(type="float")
     */
    protected $price;

    /**
     * @MongoDB\Field(type="int")
     */
    protected $stock;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $filename;

    /**
     * @MongoDB\Field(type="int")
     */
    protected $type;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }
    public function getSlug(): ?string
    {
        return (new Slugify())->slugify($this->name);
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }
    public function getFormattedPrice(): ?string
    {
        return number_format($this->price, 0, '', ' ');
    }
    public function setPrice(int $price): self
    {
        $this->price = $price;
        return $this;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }
    public function setStock(int $stock): self
    {
        $this->stock = $stock;
        return $this;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }
    public function setFilename(string $filename): self
    {
        $this->filename = $filename;
        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }
    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }
    public function getFormattedType(): ?string
    {
        return self::TYPE[$this->type];
    }

}
